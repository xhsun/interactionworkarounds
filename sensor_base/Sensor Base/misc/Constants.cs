﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Sensor_Base.misc
{
    class Constants
    {
        private static Constants constant;
        public bool isMulti = true;
        public string[] voice_key = new string[1];
        public int[] tap_key;

        public Color error_color;
        public Color white;
        /// <summary>
        /// get instance of this class
        /// since this class is singleton, constructor will not be available
        /// </summary>
        /// <returns>instance of this class</returns>
        public static Constants getInstance()
        {
            if (constant == null) { constant = new Constants(); }
            return constant;
        }
        private Constants()
        {
            byte a, r, g, b;

            if (isMulti){
                voice_key[0] = "rock paper scissors";
                tap_key = new int[4];
                tap_key[0] = 2;
                tap_key[1] = 1;
                tap_key[2] = 2;
                tap_key[3] = 2;
            }else{
                voice_key[0] = "bird";
                tap_key = new int[3];
                tap_key[0] = 1;
                tap_key[1] = 1;
                tap_key[2] = 2;
            }

            a = byte.Parse("#FFD32F2F".Substring(1, 2), NumberStyles.HexNumber);
            r = byte.Parse("#FFD32F2F".Substring(3, 2), NumberStyles.HexNumber);
            g = byte.Parse("#FFD32F2F".Substring(5, 2), NumberStyles.HexNumber);
            b = byte.Parse("#FFD32F2F".Substring(7, 2), NumberStyles.HexNumber);
            error_color = Color.FromArgb(a, r, g, b);

            a = byte.Parse("#FFFFFFFF".Substring(1, 2), NumberStyles.HexNumber);
            r = byte.Parse("#FFFFFFFF".Substring(3, 2), NumberStyles.HexNumber);
            g = byte.Parse("#FFFFFFFF".Substring(5, 2), NumberStyles.HexNumber);
            b = byte.Parse("#FFFFFFFF".Substring(7, 2), NumberStyles.HexNumber);
            white = Color.FromArgb(a, r, g, b);
        }
    }
}
