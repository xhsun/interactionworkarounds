﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Sensor_Base.Resources;
using Windows.Phone.Speech.Recognition;
using System.Windows.Input;
using Sensor_Base.misc;
using System.Windows.Media;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;


namespace Sensor_Base
{
    public partial class MainPage : PhoneApplicationPage
    {
        private Constants constant;
        //SpeechRecognizer
        private SpeechRecognizer listen;
        private SpeechRecognitionResult result;
        //fot tap key
        private List<int> total=new List<int>();

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            constant=Constants.getInstance();
            icon_init();

            //begin hint animation
            hint_shine.Begin();

            //init speech recognizer
            listen = new SpeechRecognizer();
            listen.Grammars.AddGrammarFromList("key", constant.voice_key);
            listen.AudioCaptureStateChanged += listen_AudioCaptureStateChanged;

            //when press and hold, start listening
            this.container.Hold += container_Hold;
            //when tap, start count tap key
            this.container.Tap += container_Tap;
            this.container.DoubleTap += container_DoubleTap;
            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        void container_Tap(object sender, GestureEventArgs e)
        {
            total.Add(1);
        }

        void container_DoubleTap(object sender, GestureEventArgs e)
        {
            if (total.Count > 0){total.RemoveAt(total.Count-1);}
            total.Add(2);
            if (total.Count == constant.tap_key.Length) { check_count(); }
        }

        async void container_Hold(object sender, GestureEventArgs e)
        {
            //init animatin
            icon_start();//blinking the icons
            this.hint_light.Begin();//show hint

            //listen for user
            result = await listen.RecognizeAsync();

            //check if user speak the correct key word(s)
            check_voice();
            icon_stop();//stop blinking the icons
        }

        

        void listen_AudioCaptureStateChanged(SpeechRecognizer sender, SpeechRecognizerAudioCaptureStateChangedEventArgs args)
        {
            if (args.State == SpeechRecognizerAudioCaptureState.Capturing){//promt user to speak
                this.Dispatcher.BeginInvoke(delegate { this.hint.Text = "Listening"; });
            }else if (args.State == SpeechRecognizerAudioCaptureState.Inactive){//notify user checking word
                this.Dispatcher.BeginInvoke(delegate { this.hint_light_reverse.Begin(); this.hint.Text = "Thinking"; });
            }
        }

        void check_count()
        {
            int position = 0;

            foreach (int tap in total)
            {
                if (tap != constant.tap_key[position])
                { //wrong key is given
                    wrong_key();
                    total.Clear();
                    return;
                }
                //increment position
                position++;
            }
            //all tap key check pass, pass
            pass();
        }


        private void check_voice()
        {
            //if wrong key is given, promt error message
            if (result.TextConfidence == SpeechRecognitionConfidence.Rejected) {wrong_key();}
            //didn't catch the user, promt to speak again
            else if (result.TextConfidence == SpeechRecognitionConfidence.Low) {unknow_key();}
            //correct key is given "unlock" phone
            else if (result.TextConfidence == SpeechRecognitionConfidence.High ||
                 result.TextConfidence == SpeechRecognitionConfidence.Medium) {pass();}
        }


        private async void wrong_key()
        {
            hint_shine.Begin();
            hint.Foreground = new SolidColorBrush(constant.error_color);
            this.hint.Text = "Try again";
            //show corss
            wrong_shine.Begin();
            await Task.Delay(1500);
            wrong_light_reverse.Begin();
            //change promt back
            hint.Foreground = new SolidColorBrush(constant.white);
            this.hint.Text = "Tap and Hold to Speak";
        }

        private async void unknow_key()
        {
            hint_shine.Begin();
            hint.Foreground = new SolidColorBrush(constant.error_color);
            this.hint.Text = "Didn't Catch that";
            await Task.Delay(1500);
            //change promt back
            hint.Foreground = new SolidColorBrush(constant.white);
            this.hint.Text = "Tap and Hold to Speak";
        }

        private async void timeout_promt()
        {
            hint_shine.Begin();
            hint.Foreground = new SolidColorBrush(constant.error_color);
            this.hint.Text = "Timeout";
            await Task.Delay(1500);
            //change promt back
            hint.Foreground = new SolidColorBrush(constant.white);
            this.hint.Text = "Tap and Hold to Speak";
        }

        private async void pass()
        {
            this.hint.Text = "Correct";
            await Task.Delay(500);
            Application.Current.Terminate();
        }

        private void icon_init()
        {
            if (constant.isMulti){  //multiple key, show multiple icon
                smallPic.Visibility = Visibility.Visible;
                bigPic.Visibility = Visibility.Collapsed;
            }else{  //single key, show one icon
                smallPic.Visibility = Visibility.Collapsed;
                bigPic.Visibility = Visibility.Visible;
            }
        }

        private void icon_start()
        {
            if (constant.isMulti){  //multiple key, blink small icons
                smallPic_shine.Begin();
            }else{  //single key, blink big icon
                bigPic_shine.Begin();
            }
        }

        private void icon_stop()
        {
            if (constant.isMulti){  //multiple key, stop blinking small icons
                smallPic_shine.Stop();
            }else{  //single key, stop blinking big icon
                bigPic_shine.Stop();
            }
        }

        #region stuff
        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
        #endregion stuff
    }
}