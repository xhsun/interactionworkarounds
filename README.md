# README #
Project 1: Smart Phone Interaction Workarounds

### What is this repository for? ###

This repository contains the prototype implementation of phone interaction workarounds.
For more detail on the design and walkthrough of phone interaction workarounds, please visit [my web portfolio](http://pages.cpsc.ucalgary.ca/~xhsun/work/phone.html)

### How do I get set up? ###

* You can clone or download the repository to access it from your local machine
* link for clone: git clone https://xhsun@bitbucket.org/xhsun/interactionworkarounds.git
* link for download: [link](https://bitbucket.org/xhsun/interactionworkarounds/get/6ed6f6ce3c5b.zip)
* note: if you decide to download the repository, the link for download will provide you with a zip that contain the source code

####To Run The Program####

* Open your visual studio and from there open the touch base or sensor base project. Once its loaded, you can just click on the run button to run the program.

### Contribution guidelines ###

* Author: Hannah Sun

### Who do I talk to? ###

* Hannah Sun: xhsun@ucalgary.ca