﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Touch_Base.misc;
using Windows.Phone.Devices.Notification;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace Touch_Base
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        //constants
        private static int SIZE = 40; //radius
        private static int BOUNDARY = 240;
        private constants constant;

        //vars
        private double x, y;
        public List<points> inputs = new List<points>();
        public List<Ellipse> all_line = new List<Ellipse>();
        public List<Ellipse> line = new List<Ellipse>();
        
        public MainPage()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = NavigationCacheMode.Required;
            constant = constants.getInstance();

            //begin hint animation
            hint_shine.Begin();

            //touch
            this.container.PointerPressed += containter_PointerPressed;
            this.container.PointerMoved += containter_PointerMoved;
            this.container.PointerReleased += containter_PointerReleased;
        }

        void containter_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            //init variables
            int position;
            points current = new points();
            //get x and y position
            x=e.GetCurrentPoint(this.container).Position.X;
            y=e.GetCurrentPoint(this.container).Position.Y;
            //System.Diagnostics.Debug.WriteLine("Pressed: "+x+" :: "+y);
            
            //check if point in boundary
            if (y < BOUNDARY) { return; }//not in the palette range then return

            //if reset button been pressed, rest
            if ((Math.Abs(x - constant.reset.X) < SIZE) && (Math.Abs(y - constant.reset.Y) < SIZE)) { _reset(); return; }

            //find matching dot
            for (position = 0; position < constant.dots.Length; position++)
            {
                //if x and y is in range, break
                if ((Math.Abs(x - constant.dots[position].X) < SIZE) && (Math.Abs(y - constant.dots[position].Y) < SIZE)) {
                    //find the matching dot, fill the info
                    current.isValid = true;
                    current.id = e.Pointer.PointerId;
                    current.begin = constant.dots[position];
                    inputs.Add(current);
                    return;
                }
            }
        }

        void containter_PointerMoved(object sender, PointerRoutedEventArgs e)
        {
            //get current points
            points current = inputs.Find(z => z.id == e.Pointer.PointerId);
            //get x and y position
            x = e.GetCurrentPoint(this.container).Position.X;
            y = e.GetCurrentPoint(this.container).Position.Y;

            //check pressed around a valid dot
            if (!current.isValid) return;
            //create new stroke
            Ellipse dot = new Ellipse()
            {
                Height = 40,
                Width = 40,
                Fill = new SolidColorBrush(current.begin.color),
                RenderTransform = new CompositeTransform()
            };
            //move to place
            (dot.RenderTransform as CompositeTransform).TranslateX = x - dot.Width / 2;
            (dot.RenderTransform as CompositeTransform).TranslateY = y - dot.Height / 2;
            //add to line
            line.Add(dot);
            all_line.Add(dot);
            //add to container
            this.container.Children.Add(dot);
        }

        void containter_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            //init variables
            int position;
            double current_x, distance, current_dist;
            //get current points
            points current = inputs.Find(z => z.id == e.Pointer.PointerId);
            //get x and y position
            x = e.GetCurrentPoint(this.container).Position.X;
            y = e.GetCurrentPoint(this.container).Position.Y;

            //check pressed around a valid dot
            if (!current.isValid) return;

            //find matching dot
            for (position = 0; position < constant.dots.Length; position++)
            {
                //if x and y is in range, break
                if ((constant.dots[position].X != current.begin.X) && (Math.Abs(x - constant.dots[position].X) < SIZE) && (Math.Abs(y - constant.dots[position].Y) < SIZE)) {
                    //record the end dot
                    inputs.Remove(current);
                    current.end = constant.dots[position];
                    inputs.Add(current);
                    //calculate distance
                    distance = Math.Abs(current.begin.X - current.end.X);
                    //Color color = constant.Blend(current.end.color, current.begin.color, 0.5);
                    foreach (Ellipse circle in line)
                    {
                        //calculate blend amount
                        current_x = (circle.RenderTransform as CompositeTransform).TranslateX + circle.Width / 2;
                        current_dist = Math.Abs(current.end.X - current_x);
                        //get and set color
                        circle.Fill = new SolidColorBrush(constant.Blend(current.begin.color, current.end.color, Math.Round(current_dist / distance, 2)));
                    }
                    line.Clear();
                    //check if enough key is entered
                    check();
                    return;
                }
            }

            //remove stroke
            foreach (Ellipse circle in line) { this.container.Children.Remove(circle); }
            //clear line
            line.Clear();
            //remove invalid input
            inputs.Remove(current);
        }

        private async void check()
        {
            int current=0;
            int amount = constant.keys.Length;
            //if not enough key gathered, exit check
            if (inputs.Count < amount) return;

            foreach (points point in inputs)
            {
                if ((point.begin.X != constant.keys[current].begin.X) || (point.end.X != constant.keys[current].end.X))
                {   //reset all and exit check
                    reset();
                    return;
                }
                current++;
            }
            hint.Text = "Correct!";
            await Task.Delay(700);
            Application.Current.Exit();
        }

        private async void reset()
        {
            VibrationDevice vibrate = VibrationDevice.GetDefault();
            //change hint to error message
            hint.Text = "Wrong Combination";
            hint.Foreground = new SolidColorBrush(constant.error_color);

            _reset(); //do reset
            
            //start vibration after 500 milliseconds
            await Task.Delay(300);
            vibrate.Vibrate(TimeSpan.FromMilliseconds(100));
            await Task.Delay(1700);

            //change hint back after 2 seconds
            hint.Text = "Connect colors to unlock";
            hint.Foreground = new SolidColorBrush(Windows.UI.Colors.White);
        }

        private void _reset()
        {
            //clear lines
            foreach (Ellipse circle in all_line) { this.container.Children.Remove(circle); }
            all_line.Clear();
            //clear input
            inputs = new List<points>();
        }

        #region things
        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }
        #endregion things
    }
}
