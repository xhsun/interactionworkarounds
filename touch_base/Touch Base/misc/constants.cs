﻿using System.Globalization;
using Windows.UI;

namespace Touch_Base.misc
{
    public struct color_point
    {
        public int X;
        public int Y;
        public Color color;
    }

    public struct points
    {
        public uint id;
        public bool isValid;
        public color_point begin;
        public color_point end;
    }

    class constants
    {
        private static bool isMulti = true; //false - one key, true - three key
        private static constants constant;

        public points[] keys;
        public color_point[] dots=new color_point[4];
        public Color error_color;
        public color_point reset;

        private constants()
        {
            byte a, r, g, b;
            //generate error color
            #region initColor
            a = byte.Parse("#FFD32F2F".Substring(1, 2), NumberStyles.HexNumber);
            r = byte.Parse("#FFD32F2F".Substring(3, 2), NumberStyles.HexNumber);
            g = byte.Parse("#FFD32F2F".Substring(5, 2), NumberStyles.HexNumber);
            b = byte.Parse("#FFD32F2F".Substring(7, 2), NumberStyles.HexNumber);
            error_color = Color.FromArgb(a, r, g, b);
            
            dots[0].X = 79;
            dots[0].Y = 356;
            //convert color RED
            a = byte.Parse("#FFFF4081".Substring(1, 2), NumberStyles.HexNumber);
            r = byte.Parse("#FFFF4081".Substring(3, 2), NumberStyles.HexNumber);
            g = byte.Parse("#FFFF4081".Substring(5, 2), NumberStyles.HexNumber);
            b = byte.Parse("#FFFF4081".Substring(7, 2), NumberStyles.HexNumber);
            dots[0].color = Color.FromArgb(a, r, g, b);

            dots[1].X = 167;
            dots[1].Y = 295;
            //convert color BLUE
            a = byte.Parse("#FF00BCD4".Substring(1, 2), NumberStyles.HexNumber);
            r = byte.Parse("#FF00BCD4".Substring(3, 2), NumberStyles.HexNumber);
            g = byte.Parse("#FF00BCD4".Substring(5, 2), NumberStyles.HexNumber);
            b = byte.Parse("#FF00BCD4".Substring(7, 2), NumberStyles.HexNumber);
            dots[1].color = Color.FromArgb(a, r, g, b);

            dots[2].X = 273;
            dots[2].Y = 314;
            //convert color ORANGE
            a = byte.Parse("#FFFF9800".Substring(1, 2), NumberStyles.HexNumber);
            r = byte.Parse("#FFFF9800".Substring(3, 2), NumberStyles.HexNumber);
            g = byte.Parse("#FFFF9800".Substring(5, 2), NumberStyles.HexNumber);
            b = byte.Parse("#FFFF9800".Substring(7, 2), NumberStyles.HexNumber);
            dots[2].color = Color.FromArgb(a, r, g, b);

            dots[3].X = 329;
            dots[3].Y = 403;
            //convert color GREEN
            a = byte.Parse("#FF8BC34A".Substring(1, 2), NumberStyles.HexNumber);
            r = byte.Parse("#FF8BC34A".Substring(3, 2), NumberStyles.HexNumber);
            g = byte.Parse("#FF8BC34A".Substring(5, 2), NumberStyles.HexNumber);
            b = byte.Parse("#FF8BC34A".Substring(7, 2), NumberStyles.HexNumber);
            dots[3].color = Color.FromArgb(a, r, g, b);
            #endregion initColor
            reset = new color_point();
            reset.X = 200;
            reset.Y = 466;
            keyGenerate();
        }

        /// <summary>
        /// get instance of this class
        /// since this class is singleton, constructor will not be available
        /// </summary>
        /// <returns>instance of this class</returns>
        public static constants getInstance()
        {
            if (constant == null) { constant = new constants(); }
            return constant;
        }

        /// <summary>Blends the specified colors together.
        /// code from: http://stackoverflow.com/questions/3722307/is-there-an-easy-way-to-blend-two-system-drawing-color-values
        /// </summary>
        /// <param name="color">Color to blend onto the background color.</param>
        /// <param name="backColor">Color to blend the other color onto.</param>
        /// <param name="amount">How much of <paramref name="color"/> to keep,
        /// “on top of” <paramref name="backColor"/>.</param>
        /// <returns>The blended colors.</returns>
        public Color Blend(Color color, Color backColor, double amount)
        {
            byte a = (byte)((color.A * amount) + backColor.A * (1 - amount));
            byte r = (byte)((color.R * amount) + backColor.R * (1 - amount));
            byte g = (byte)((color.G * amount) + backColor.G * (1 - amount));
            byte b = (byte)((color.B * amount) + backColor.B * (1 - amount));
            return Color.FromArgb(a, r, g, b);
        }

        private void keyGenerate()
        {
            if (isMulti)
            {
                keys = new points[3];
                //first key: Red to Green
                points key1 = new points();
                key1.begin = dots[0];
                key1.end = dots[3];
                keys[0] = key1;

                //second key: red to orange
                points key2 = new points();
                key2.begin = dots[0];
                key2.end = dots[2];
                keys[1] = key2;

                //last key: orange to blue
                points key3 = new points();
                key3.begin = dots[2];
                key3.end = dots[1];
                keys[2] = key3;
            }
            else
            {
                keys = new points[1];
                //first key: Blue to Green
                points key1 = new points();
                key1.begin = dots[1];
                key1.end = dots[3];
                keys[0] = key1;
            }
        }
    }
}
